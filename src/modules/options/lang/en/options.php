<?php

return [
    
    "module" => "options",
    
    'options' => 'Options',

    'main' => 'Main settings',
    'seo' => 'SEO',
    'modules' => 'Modules',
    'social' => 'Social pages',
    'media' => 'Media',
    'plugins' => 'Plugins',
    'change_logo' => 'Change site logo',
    'remove_logo' => 'Remove site logo',
    'chane_logo_help' => 'appears when sharing',
    'amazon_integration' => 'Amazon integration',
    'kilobytes' => 'KB',
    'pixels' => 'Pixels',
    'save_options' => 'Save options',
    'sitemap' => 'Sitemap',
    'update_sitemap' => 'Update',
    'updating_sitemap' => 'Updating',
    'not_writable' => 'is not writable',
    'install' => 'Install',
    'uninstall' => 'Uninstall',
    'sure_uninstall_plugin' => 'Are you sure to uninstall :name plugin ?',
    'sure_install_plugin' => 'Are you sure to install :name plugin ?',
    'no_plugins' => 'No Plugins found',
    'file_not_supported' => 'File is not allowed',


    "dot_version" => "Dot platform version",
    "check_for_update" => "Check for updates",
    "checking" => "Checking..",
    "how_update" => "How to update",
    "version_available" => "Dot platform <strong style='font-family: sans-serif, Arial, Verdana'>:version</strong> is available !",
    "up_to_date" => "Dot platform is up to date.",

    'resize_mode_resize' => 'Resize with aspect ratio',
    'resize_mode_resize_crop' => 'Smart Resize and crop',
    'resize_mode_color_background' => 'Resize with background color',
    'resize_mode_gradient_background' => 'Resize with gradients background',
    'resize_mode_blur_background' => 'Resize with blur background',

    'attributes' => [
        'site_name' => 'Site name',
        'site_slogan' => 'Site slogan',
        'site_email' => 'Site email',
        'site_copyrights' => 'Site copyrights',
        'timezone' => 'Time zone',
        'date_format' => 'Date format',
        'locale' => 'Locale',
        'site_status' => 'Site status',
        'offline_message' => 'Offline message',
        'site_title' => 'Site title',
        'site_description' => 'Site description',
        'site_author' => 'Site author',
        'site_robots' => 'Site_robots',
        'site_keywords' => 'Site keywords',
        'site_logo' => 'Site logo',
        'sitemap_path' => 'Sitemap path',
        'sitemap_limit' => 'Sitemap limit',
        'allowed_file_types' => 'Allowed file types',
        'max_file_size' => 'Max file size',
        'max_width' => 'Max width',
        'media_thumbnails' => 'Create Image thumbnails',
        'media_cropping' => 'Enable cropping',
        'media_watermarking' => 'Enable watermarking',
        's3_status' => 'Enable S3 upload',
        's3_bucket' => 'Bucket name',
        's3_region' => 'Region name',
        's3_delete_locally' => 'Delete file locally after uploading to S3',
        'sitemap_last_update' => 'Last update in ',
        'sitemap_status' => 'Status',
        'sitemap_xml_status' => 'XML sitemap',
        'sitemap_html_status' => 'HTML sitemap',
        'sitemap_txt_status' => 'TXT sitemap',
        'sitemap_ping' => 'Ping status',
        'sitemap_google_ping' => 'Google',
        'sitemap_bing_ping' => 'Bing',
        'sitemap_yahoo_ping' => 'Yahoo',
        'sitemap_ask_ping' => 'Ask',
        
        'facebook_page' => 'Facebook',
        'twitter_page' => 'Twitter',
        'googleplus_page' => 'Google plus',
        'youtube_page' => 'Youtube',
        'instagram_page' => 'Instagram',
        'linkedin_page' => 'Linked in',
        'soundcloud_page' => "Soundcloud",


        'resize_mode' => 'Resize mode',
        'resize_background_color' => 'Background color',
        'resize_gradient_first_color' => 'First color',
        'resize_gradient_second_color' => 'Second color'

    ],


    "events" => [
        'saved' => 'Options saved successfully',
        'installed' => "Plugin installed successfully",
        'uninstalled' => "Plugin uninstalled successfully"
    ],

    "permissions" =>[
        "general" => "Manage general options",
        "seo" => "Manage SEO options",
        "social" => "Manage social pages",
        "media" => "Manage media options",
        "plugins" => "Manage plugins",
    ]
];

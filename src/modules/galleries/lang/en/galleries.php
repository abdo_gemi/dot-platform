<?php

return [
    "galleries" => "Galleries",
    "edit" => "Edit Gallery",
    "search" => "search",
    "no_media_on" => "No media on ",
    "add_new" => "Add new",
    "per_page" => "Per page",
    "bulk_actions" => "Bulk actions",
    "apply" => "Apply",
    "delete" => "Delete",
    "name" => "Gallery name",
    "actions" => "Action",
    "sure_delete" => "you are about to delete ... continue?",
    "page" => "Page",
    "add_new_gallery" => "Add new gallery",
    "add_media" => "Add media",
    "author" => "Gallery author",
    "no_media" => "No media has been added",
    "save_gallery" => "Save gallery",
    "is_not_valid_image" => "is not an image",
    "of" => "of",
    "search_galleries" => "Seach galleries",
    "files" => "files",
    "bulk_actions" => "Bulk actions",

    "media_row_delete" => "Are you sure to delete this file",
    "delete_gallery" => "Are you sure to delete this gallery",


    "media_count" => "Files length",
    "no_galleries" => "No galleries",



    "attributes" => [
        "name" => "Name",
        "author" => "Author"
    ],


    "events" => [
        "created" => "Gallery created successfully",
        "updated" => "Gallery updated successfully",
        "deleted" => "Gallery deleted successfully",
    ],
    "permissions" => [
        "manage" => "Manage galleries"
    ]
];

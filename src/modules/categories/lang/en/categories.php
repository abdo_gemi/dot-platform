<?php
return [

    'module' => 'categories',

    'categories' => 'Categories',
    'category' => 'category',
    'add_new' => 'Add new category',
    'edit' => 'Edit category',
    'back_to_categories' => 'Back to categories',
    'no_records' => 'No categories found',
    'save_category' => 'save category',
    'search' => 'search',
    'search_categories' => 'Search categories',
    'per_page' => 'Per page',
    'bulk_actions' => 'Bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    'language' => 'Language',
    'parent_category' => 'main category',
    'show_children' => 'Show sub categories',
    'sure_delete' => 'Are you sure to delete ?',
    'change_image' => 'change image',
    'add_image' => 'Add image',
    'not_allowed_file' => 'not an allowed file type',

    'attributes' => [
        
        'category_category_name' => 'category_category_name',
        'name' => 'Name',
        'slug' => 'slug',
        'parent' => 'parent',
        
        
    ],

    "events" => [
        'created' => 'Category created successfully',
        'updated' => 'Category updated successfully',
        'deleted' => 'Category deleted successfully',
        
    ],
    "permissions" => [
        "manage" => "Manage categories"
    ]
    
];

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function ($table) {
            $table->increments('id');
            $table->integer("parent")->default(0)->index();
            $table->string("name")->index();
            $table->string("slug")->index();
            $table->integer("image_id")->default(0)->index();
            $table->integer("user_id")->default(0)->index();
            $table->string("lang")->index();
            $table->string("status")->default(0)->index();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}

<?php
return [

    'module' => 'blocks',
    'blocks' => 'Blocks',
    'block' => 'block',
    'add_new' => 'Add new block',
    'edit' => 'Edit block',
    'back_to_blocks' => 'Back to blocks',
    'no_records' => 'No blocks found',
    'save_block' => 'save block',
    'search' => 'search',
    'search_blocks' => 'Search blocks',
    'per_page' => 'Per page',
    'bulk_actions' => 'Bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'filter',
    "related_blocks" => "Related blocks",
    'all_categories' => 'All categories',
    "filter" => "Filter",
    'sure_delete' => 'Are you sure to delete ?',

    "all" => "All blocks",
    "private" => "Private blocks",

    "yes" => "Yes",
    "no" => "No",

    "remove_help" => "You are about to delete block<br/>Please select another block to transfer deleted block posts ..",
    "close" => "Close",

    "master" => "master",
    "slave" => "slave",

    "cannot_delete_master" => "You can not delete master block",
    "required_master_block" => "master block required",
    "delete_block" => "Delete block",
    'category_category.id' => 'category_category.id',
    'category_cat_name' => 'category_cat_name',
    "not_categorized" => "Not categorized",

    "master_blocks" => "Master blocks",

    'type_post' => "post",
    'type_category' => "category",
    'type_tag' => "tag",
    'type_program' => "program",
    'type_presenter' => "presenter",
    'type_live' => "live stream",

    'attributes' => [
        "type" => "Type",
        'name' => 'Name',
        'limit' => "Limit"
    ],
    "events" => [
        'created' => 'Block created successfully',
        'updated' => 'Block updated successfully',
        'deleted' => 'Block deleted successfully',

    ],
    "permissions" => [
        "manage" => "Manage blocks"
    ]

];

<?php

return [


    'posts' => 'Posts',
    'search_posts' => "Search posts",
    'pages' => 'Pages',
    'search_pages' => "Search pages",
    'categories' => 'Categories',
    'search_categories' => "Search categories",
    'tags' => 'Tags',
    'search_tags' => "Search tags",
    'save_items' => 'Save items',

    "add_new" => "Add new menu",
    "edit_menu" => "Edit menu",
    "delete_menu" => "Delete menu",

    "no_menus" => "No menus found",

    "confirm_menu_delete" => "Are you sure to delete menu ?",
    "confirm_item_delete" => "Are you sure to delete item ?",

    "save" => "Save",

    "close" => "Cancel",

    "loading" => "Loading..",

    'no_results' => "No results",

    "name" => "Name",
    "add_item" => "Add item",
    "links" => "Links",


    'no_image' => "No image",

    'module' => 'Navigations',

    'events' => [
        // Actions messages
    ],

    'attributes' => [
        "name" => "Name"
    ],

    'permissions' => [
        "manage" => "Manage navigations menus"
    ]

];

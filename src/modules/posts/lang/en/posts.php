<?php
return [

    'module' => 'Posts',

    'posts' => 'Posts',
    'post' => 'post',
    'add_new' => 'Add new post',
    'edit' => 'Edit post',
    'back_to_posts' => 'Back to posts',
    'no_records' => 'No posts found',
    'save_post' => 'Save post',
    'search' => 'search',
    'search_posts' => 'Search posts',
    'per_page' => 'Per page',
    'bulk_actions' => 'Bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'post' => 'Post',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',

    'from' => "From",
    'to' => "To",

    'post_status' => 'Post status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate post ?",
    'sure_deactivate' => "Are you sure to deactivate post ?",
    'sure_delete' => 'Are you sure to delete ?',
    
    'add_image' => 'Add image',
    'change_image' => 'change image',
    'change_media' => 'change media',

    'add_media' => "Add media",

    'not_image_file' => 'Please select a valid image file',
    'not_media_file' => 'Please select a valid video file',

    'user' => 'User',
    'tags' => 'Tags',
    'add_tag' => 'Add tags',
    'templates' => 'Templates',

    'all_categories' => "All categories",
    'all_blocks' => "All blocks",
    'all_formats' => "All formats",
    'add_category' => "Add to category",

    "format_post" => "Post",
    "format_article" => "Article",
    "format_video" => "Video",
    "format_album" => "Album",
    "format_event" => "Event",

    "add_fields" => "Add custom fields",
    "custom_name" => "Name",
    "custom_value" => "Value",
    "sure_delete_field" => "Are you sure to delete custom field ?",

    "add_block" => "Add to blocks",
    "no_blocks" => "No blocks found",

    "add_gallery" => "Add gallery",
    "no_galleries_found" => "No galleries found",

    'attributes' => [
        'title' => 'Title',
        'excerpt' => 'Excerpt',
        'content' => 'Content',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',
        'published_at' => 'Event date',
        'status' => 'Status',
        'template' => 'Template',
        'default' => 'Default',
        "format" => "Post format"
    ],

    "events" => [
        'created' => 'Post created successfully',
        'updated' => 'Post updated successfully',
        'deleted' => 'Post deleted successfully',
        'activated' => 'Post activated successfully',
        'deactivated' => 'Post deactivated successfully'
    ],

    "permissions" => [
        "manage" => "Manage posts"
    ]
    
];

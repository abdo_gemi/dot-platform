<?php

class PostMeta extends Dot\Model{

    protected $table = "posts_meta";

    public $timestamps = false;

}
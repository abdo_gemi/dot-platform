<?php
return [

    'module' => 'pages',

    'pages' => 'Pages',
    'page' => 'page',
    'add_new' => 'Add new page',
    'edit' => 'Edit page',
    'back_to_pages' => 'Back to pages',
    'no_records' => 'No pages found',
    'save_page' => 'save page',
    'search' => 'search',
    'search_pages' => 'Search pages',
    'per_page' => 'Per page',
    'bulk_actions' => 'Bulk actions',
    'delete' => 'delete',
    'apply' => 'apply',
    'page' => 'Page',
    'of' => 'of',
    'order' => 'Order',
    'sort_by' => 'Sort by',
    'asc' => 'Ascending',
    'desc' => 'Descending',
    'actions' => 'Actions',
    'filter' => 'Filter',
    
    'page_status' => 'Page status',
    'activate' => 'activate',
    'activated' => 'activated',
    'all' => 'All',
    'deactivate' => 'deactivate',
    'deactivated' => 'deactivated',
    'sure_activate' => "Are you sure to activate page ?",
    'sure_deactivate' => "Are you sure to deactivate page ?",
    'sure_delete' => 'Are you sure to delete ?',
    
    'add_image' => 'Add image',
    'change_image' => 'change image',
    'not_allowed_file' => 'File is not allowed',
    'user' => 'User',
    'tags' => 'Tags',
    'add_tag' => 'Add tags',
    'templates' => 'Templates',

    'attributes' => [
        'title' => 'Title',
        'slug' => "Slug",
        'excerpt' => 'Excerpt',
        'content' => 'Content',
        'created_at' => 'Created date',
        'updated_at' => 'Updated date',
        'status' => 'Status',
        'template' => 'Template',
        'default' => 'Default'
    ],

    "events" => [
        'created' => 'Page created successfully',
        'updated' => 'Page updated successfully',
        'deleted' => 'Page deleted successfully',
        'activated' => 'Page activated successfully',
        'deactivated' => 'Page deactivated successfully'
    ],

    "permissions" => [
        "manage" => "Manage pages"
    ]
    
];

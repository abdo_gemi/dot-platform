<?php

return [

    'module' => '#module|ucfirst#',

    'events' => [
        // Actions messages
    ],

    'attributes' => [
        // Model attributes
    ],

    'permissions' => [
        // Permissions translations
    ]

];

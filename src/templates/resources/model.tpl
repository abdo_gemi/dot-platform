<?php

class #model|ucfirst# extends Dot\Model {

    protected $table = '#table#';

    /**
     * Creating rules
     * @var array
     */
    protected $creatingRules = [

    ];

    /**
     * Updating rules
     * @var array
     */
    protected $updatingRules = [

    ];

}